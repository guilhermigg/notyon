# 🔑 How to get your Notion auth token

### To get your authentication token you need to to create an integration.
### Log into your notion account, then access https://www.notion.so/my-integrations

![Click on "Create new integration"](https://i.ibb.co/XxX01zs/notion-token-1.png)

----------

### ✍️ Now you can set your Integration name (of your choice)
![Insert a name to your integration](https://i.ibb.co/tK7k1wt/notion-token-2.png)

----------

### 🔑 Here you can choose the stuff you are going to deal. Allow only what you are going to use.
![Allow what you think is necessary](https://i.ibb.co/BnyKXyD/notion-token-3.png)

----------

### Now you have your auth token, ready to be copied!
### ⚠️ Don't share this token! Only you should have access to this token
![Click on "Create new integration"](https://i.ibb.co/wRXzZfk/notion-token-4.png)